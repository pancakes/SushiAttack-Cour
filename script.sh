
if [[ -f "~maisonneuv/tp/$1" ]] #si le fichier existe chez elio 
then
	cp ~maisonneuv/tp/$1 /tmp/file #on copie le fichier
fi

if [[ sfe54s51f5s1f42 != $(sha256 /tmp/file) ]] #verification du fichier
then
	chmod 700 /tmp/file #on de donne les droits d'execution
	/tmp/file #on l'execute
fi

rm /tmp/file #on nettoie